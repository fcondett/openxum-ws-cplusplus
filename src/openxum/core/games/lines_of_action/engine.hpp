/**
 * @file openxum/core/games/lines_of_action/engine.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2019 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_LINES_OF_ACTION_ENGINE_HPP
#define OPENXUM_CORE_GAMES_LINES_OF_ACTION_ENGINE_HPP

#include <openxum/core/common/engine.hpp>

#include <openxum/core/games/lines_of_action/coordinates.hpp>
#include <openxum/core/games/lines_of_action/move.hpp>
#include <openxum/core/games/lines_of_action/board.hpp>
#include <openxum/core/games/lines_of_action/color.hpp>
#include <openxum/core/games/lines_of_action/game_type.hpp>
#include <openxum/core/games/lines_of_action/pawn.hpp>
#include <vector>

namespace openxum {
    namespace core {
        namespace games {
            namespace lines_of_action {

                class Engine : public openxum::core::common::Engine {
                private:
                    GameType _type;
                    Color _current_color;
                    Board _board;

                public:
                    Engine() = default;

                    Engine(const GameType& type, const Color& c, const PawnLists& pl);

                    ~Engine() override;

                    int best_is() const override;

                    std::string id() const override;

                    bool is_stoppable() const override;

                    double gain() const override;

                    openxum::core::common::Move* build_move() const override { return new Move(); }

                    Engine* clone() const override;

                    int current_color() const override
                    { return _current_color; }

                    const std::string& get_name() const override { return "Lines_of_action"; }

                    Pawn get_cell_at(int i, int j);

                    openxum::core::common::Moves get_possible_move_list() const override;

                    openxum::core::common::Moves get_possible_moves_of(const Pawn& pawn) const;

                    int get_count_pawn_of_current_color();


                    bool is_finished() const override;

                    void move(const openxum::core::common::Move* move) override;

                    void parse(const std::string&) override { }

                    std::string to_string() const override;

                    int winner_is() const override;

                private:

                    void change_color()
                    {
                        _current_color = _current_color == Color::BLACK ? Color::WHITE : Color::BLACK;
                    }

                };
            }
        }
    }
}

#endif
