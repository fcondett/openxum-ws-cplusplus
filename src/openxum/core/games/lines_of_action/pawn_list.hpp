/**
 * @file openxum/core/games/lines_of_action/pawn_list.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2019 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_lines_of_action_PAWN_LIST_HPP
#define OPENXUM_CORE_GAMES_lines_of_action_PAWN_LIST_HPP

#include <openxum/core/common/move.hpp>

#include <string>
#include <vector>
#include <openxum/core/games/lines_of_action/pawn.hpp>
#include <openxum/core/games/lines_of_action/color.hpp>
#include <openxum/core/games/lines_of_action/coordinates.hpp>
#include <openxum/core/games/lines_of_action/move.hpp>

namespace openxum {
    namespace core {
        namespace games {
            namespace lines_of_action {

                class PawnLists {
                private:
                    std::vector<Pawn> _list;
                    int _count_black_pawn;
                    int _count_white_pawn;
                    int _nb_pawn;

                public:
                    PawnLists();
                    ~PawnLists();
                    void initialise_list();
                    void push_pawn(const Pawn& pawn);
                    int count_black_pawn() const;
                    int count_white_pawn() const;
                    int nb_pawn() const;
                    Pawn get_pawn(int index) const;
                    std::vector<Pawn> get_pawns_of(const Color& color);
                    Color find_pawn(const Coordinates& coord) const;
                    void change_coord_pawn(const Coordinates& coord1, const Coordinates& coord2);
                    void delete_pawn(const Coordinates& coord);
                    openxum::core::common::Moves get_possible_moves_list_player(const Color& color_player) const;
                    std::vector<Move*> get_moves_of_current_pawn(const Pawn& current_pawn) const;
                    bool check_not_blocked_vertical_path(const Coordinates& current_coord,
                                                    const Coordinates& potential_coord,
                                                    const Color& opponent_color, std::string side) const;
                    void add_vertical_moves(const Coordinates& current_pawn_coordinates,
                                            const Color& current_pawn_color, std::vector<Move*>
                                            moves_list) const;
                    bool check_not_blocked_horizontal_path(const Coordinates& current_coord,
                                                    const Coordinates& potential_coord,
                                                    const Color& opponent_color, std::string side) const;
                    void add_horizontal_moves(const Coordinates& current_pawn_coordinates,
                                            const Color& current_pawn_color,
                                              std::vector<Move*>
                                            moves_list) const;
                    bool check_not_blocked_diagonal1_path(const Coordinates& current_coord,
                                                    const Coordinates& potential_coord,
                                                    const Color& opponent_color, std::string side) const;
                    void add_diagonal1_moves(const Coordinates& current_pawn_coordinates,
                                            const Color& current_pawn_color, std::vector<Move*>
                                            moves_list) const;
                    bool check_not_blocked_diagonal2_path(const Coordinates& current_coord,
                                                    const Coordinates& potential_coord,
                                                    const Color& opponent_color, std::string side) const;
                    void add_diagonal2_moves(const Coordinates& current_pawn_coordinates,
                                            const Color& current_pawn_color, std::vector<Move*>
                                            moves_list) const;

                };

            }
        }
    }
}

#endif
