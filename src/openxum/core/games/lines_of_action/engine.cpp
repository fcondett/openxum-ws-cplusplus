/**
 * @file openxum/core/games/lines_of_action/engine.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2019 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <openxum/core/games/lines_of_action/engine.hpp>
#include <openxum/core/games/lines_of_action/move.hpp>
#include <openxum/core/games/lines_of_action/coordinates.hpp>
#include <openxum/core/games/lines_of_action/board.hpp>
#include <openxum/core/games/lines_of_action/color.hpp>

namespace openxum {
    namespace core {
        namespace games {
            namespace lines_of_action {
            Engine::Engine(const GameType& t, const Color& c, const PawnLists& pl) {
                    //super();
                    _type = t;
                    _current_color = c;
                    _board = Board(pl);
                }

            Engine* Engine::clone() const
            {
                GameType type = _type;
                Color current_color = _current_color;
                Board board = _board;
                PawnLists pawnlist = board.pawnlists();
                auto e = new Engine(type, current_color, pawnlist);
                return e;
            }

            Pawn Engine::get_cell_at(int i, int j){
                return _board.get_pawn_at(i,j);
            }

            openxum::core::common::Moves  Engine::get_possible_move_list() const {
                return _board.get_possible_moves_of_player(Color(current_color()));
            }

            openxum::core::common::Moves  Engine::get_possible_moves_of(const Pawn& pawn) const {
                return _board.get_possible_moves_of(pawn);
            }

            bool Engine::is_finished() const {
                Pawn last_pawn_moved = *_board.pick_a_pawn_of(Color(current_color()));
                return _board.is_winning_pawn(last_pawn_moved);
            }

            void Engine::move(const openxum::core::common::Move* move) {
                auto* m = dynamic_cast<const openxum::core::games::lines_of_action::Move*>(move);
                _board.move_pawn(*m);
                if (!is_finished()){
                    change_color();}
            }

            int Engine::winner_is() const {
                if(is_finished()){return _current_color;}
                else{ return NONE;}

            }

            }
        }
    }
}
