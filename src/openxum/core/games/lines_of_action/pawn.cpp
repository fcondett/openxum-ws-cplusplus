/**
 * @file openxum/core/games/lines_of_action/pawn.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2019 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <openxum/core/games/lines_of_action/pawn.hpp>
#include <string>

namespace openxum {
    namespace core {
        namespace games {
            namespace lines_of_action {
            Pawn::Pawn(const Color& col, const Coordinates& coord){
                _coordinates = coord;
                _color = col;
            }

            Coordinates Pawn::get_coordinates() const{
                return _coordinates;
            }

            void Pawn::set_coordinates(const Coordinates& coord){
                _coordinates = coord;
            }

            Color Pawn::get_color() const{
                return _color;
            }

            void Pawn::set_color(const Color& color){
                _color = color;
            }

            }
        }
    }
}
