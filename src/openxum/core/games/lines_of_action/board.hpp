/**
 * @file openxum/core/games/lines_of_action/board.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2019 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_LINES_OF_ACTION_BOARD_HPP
#define OPENXUM_CORE_GAMES_LINES_OF_ACTION_BOARD_HPP

//#include <openxum/core/common/move.hpp>
#include <openxum/core/games/lines_of_action/move.hpp>
#include <openxum/core/games/lines_of_action/coordinates.hpp>
#include <openxum/core/games/lines_of_action/pawn_list.hpp>
#include <openxum/core/games/lines_of_action/pawn.hpp>
#include <openxum/core/games/lines_of_action/dimension.hpp>
#include <openxum/core/games/lines_of_action/color.hpp>

#include <vector>

namespace openxum {
    namespace core {
        namespace games {
            namespace lines_of_action {

                class Board {
                private:
                    PawnLists _pawnList;
                    std::vector<std::vector<Pawn*> > _board;
                    int _width;
                    int _height;

                public:
                    Board();
                    Board(const PawnLists& pl);

                    int getWidth() const;
                    int getHeight() const;
                    PawnLists pawnlists();
                    Pawn get_pawn_at(int i, int j) const;
                    void move_pawn(const Move& move);
                    void delete_pawn(const Coordinates& coord);
                    openxum::core::common::Moves get_possible_moves_of_player(const Color& color_player) const;
                    openxum::core::common::Moves get_possible_moves_of(const Pawn& pawn) const;
                    bool is_winning_pawn(const Pawn& last_pawn_moved) const;
                    Pawn* pick_a_pawn_of(const Color& color) const;


                };

            }
        }
    }
}

#endif
