/**
 * @file openxum/core/games/lines_of_action/connected_pawns.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2019 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_LINES_OF_ACTION_CONNECTED_PAWNS_HPP
#define OPENXUM_CORE_GAMES_LINES_OF_ACTION_CONNECTED_PAWNS_HPP

#include <string>
#include <vector>
#include <openxum/core/games/lines_of_action/pawn.hpp>
#include <openxum/core/games/lines_of_action/board.hpp>
#include <openxum/core/games/lines_of_action/color.hpp>

namespace openxum {
    namespace core {
        namespace games {
            namespace lines_of_action {

            class ConnectedPawns {
            private:
                std::vector<Pawn*> _neighbors;
                std::vector<Pawn*> _visited;
                std::vector<Pawn*> _wait;
                std::vector<std::vector<Pawn*> > _board;
                Pawn _lpm;
                Color _pawns_color;
                int _count_of_pawn;

            public:
                ConnectedPawns(const Pawn& last_pawn_moved, int count_of_pawn, std::vector<std::vector<Pawn*> > board);
                ~ConnectedPawns();
                bool all_pawns_connected();

            private:
                void _get_neighbors_of(const Pawn& current_pawn);
                bool _is_visited(const Pawn& pawn);
                bool _is_waiting(const Pawn& pawn);
                Pawn _pop_wait();
                void _add_wait();
                bool _wait_is_empty();
            };

            }
        }
    }
}

#endif
