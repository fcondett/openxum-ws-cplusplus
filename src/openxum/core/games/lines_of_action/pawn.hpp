/**
 * @file openxum/core/games/lines_of_action/phase.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2019 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_LINES_OF_ACTION_PAWN_HPP
#define OPENXUM_CORE_GAMES_LINES_OF_ACTION_PAWN_HPP

#include <openxum/core/games/lines_of_action/color.hpp>
#include <openxum/core/games/lines_of_action/coordinates.hpp>
#include <string>

namespace openxum {
    namespace core {
        namespace games {
            namespace lines_of_action {

            class Pawn{
            private:
                Color _color;
                Coordinates _coordinates;

            public:
                Pawn();
                Pawn(const Color& col, const Coordinates& coord);

                Coordinates get_coordinates() const;
                void set_coordinates(const Coordinates& coord);
                Color get_color() const;
                void set_color(const Color& color);

                std::string to_string() const {
                    if(_color == BLACK)
                    {
                        return std::string("[color: ") + "black " + " coord: " + _coordinates.to_string() + "]";
                    }
                    if(_color == WHITE)
                    {
                        return std::string("[color: ") + "white " + " coord: " + _coordinates.to_string() + "]";
                    }
                }
            };
        }
    }
}
}

#endif
