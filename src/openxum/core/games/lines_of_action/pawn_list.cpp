/**
 * @file openxum/core/games/lines_of_action/pawn_list.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2019 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <openxum/core/games/lines_of_action/pawn_list.hpp>
#include <openxum/core/games/lines_of_action/color.hpp>
#include <openxum/core/games/lines_of_action/dimension.hpp>
#include <openxum/core/games/lines_of_action/coordinates.hpp>
#include <openxum/core/games/lines_of_action/move.hpp>

namespace openxum {
    namespace core {
        namespace games {
            namespace lines_of_action {
            PawnLists::PawnLists(){
                _count_black_pawn = 12;
                _count_white_pawn = 12;
                _nb_pawn = 24;

                initialise_list();
            }

            void PawnLists::initialise_list() {

                for (int count_top_pawn = 0; count_top_pawn < 6; count_top_pawn++) {
                    _list.push_back(Pawn(BLACK, Coordinates(0, count_top_pawn + 1)));
                }

                for (int count_bottom_pawn = 0; count_bottom_pawn < 6; count_bottom_pawn++) {
                    _list.push_back(Pawn(BLACK, Coordinates(DEFAULT_WIDTH - 1, count_bottom_pawn + 1)));
                }

                for (int count_left_pawn = 0; count_left_pawn < 6; count_left_pawn++) {
                    _list.push_back(Pawn(WHITE, Coordinates(count_left_pawn + 1, 0)));
                }

                for (int count_right_pawn = 0; count_right_pawn < 6; count_right_pawn++) {
                    _list.push_back(Pawn(WHITE, Coordinates(count_right_pawn + 1, DEFAULT_HEIGHT - 1)));
                }
            }

            void PawnLists::push_pawn(const Pawn& pawn) {
                _list.push_back(pawn);
            }

            int PawnLists::count_black_pawn() const {
                return _count_black_pawn;
            }

            int PawnLists::count_white_pawn() const {
                return _count_white_pawn;
            }

            int PawnLists::nb_pawn() const {
                return _nb_pawn;
            }

            Pawn PawnLists::get_pawn(int index) const {
                return _list[index];
            }

            std::vector<Pawn> PawnLists::get_pawns_of(const Color& color){
                std::vector<Pawn> pawns;

                for(int i = 0; i < _nb_pawn; i++){
                    if(get_pawn(i).get_color() == color){
                        pawns.push_back(get_pawn(i));
                    }
                }

                return pawns;
            }

            Color PawnLists::find_pawn(const Coordinates& coord) const{
                for (int i = 0; i < nb_pawn(); i++) {
                    Pawn current_pawn = get_pawn(i);
                    if (coord.y() == current_pawn.get_coordinates().y() && coord.x() == current_pawn.get_coordinates().x()) {
                        return current_pawn.get_color();
                    }
                }
                return NONE;
            }

            void PawnLists::change_coord_pawn(const Coordinates& coord1, const Coordinates& coord2) {
                for (int i = 0; i < nb_pawn(); i++) {
                    Pawn current_pawn = get_pawn(i);
                    if (coord1.y() == current_pawn.get_coordinates().y() && coord1.x() == current_pawn.get_coordinates().x()) {
                        _list[i].set_coordinates(coord2);
                        return;
                    }
                }
            }

            void PawnLists::delete_pawn(const Coordinates& coord) {
                for(unsigned int i = 0 ; i < _list.size(); i++){
                    Coordinates current_pawn_coordinate = _list[i].get_coordinates();
                    if (coord.y() == current_pawn_coordinate.y() && coord.x() == current_pawn_coordinate.x()) {
                        _nb_pawn--;

                        if (get_pawn(i).get_color() == BLACK) {
                            _count_black_pawn--;
                        }
                        if (get_pawn(i).get_color() == WHITE) {
                            _count_white_pawn--;
                        }
                        _list.erase(_list.begin() + i);
                        return;
                    }
                }
            }

            openxum::core::common::Moves PawnLists::get_possible_moves_list_player(const Color& color_player) const{
                int count_of_pawn = 0;
                int count_of_pawn_index = 0;
                std::vector<Move*> list_new_coord;
                Move* current_move;
                openxum::core::common::Moves moves_list_player;

                if (color_player == BLACK) {
                    count_of_pawn = count_black_pawn();
                }
                else if (color_player == WHITE) {
                    count_of_pawn = count_white_pawn();
                }

                for(int i = 0; i < nb_pawn(); i++){
                    if(get_pawn(i).get_color() == color_player){
                        list_new_coord = get_moves_of_current_pawn(get_pawn(i));
                        count_of_pawn_index++;
                        for(unsigned int j = 0; j < list_new_coord.size(); j++){
                            current_move = new Move(get_pawn(i).get_color(), get_pawn(i).get_coordinates(), list_new_coord[j]->from());
                            moves_list_player.push_back(current_move);
                        }
                    }
                    if(count_of_pawn_index == count_of_pawn){
                        return moves_list_player;
                    }
                }
            return moves_list_player;
            }

            std::vector<Move*> PawnLists::get_moves_of_current_pawn(const Pawn& current_pawn) const {
                    std::vector<Move*> moves_list;
                    Coordinates current_pawn_coordinates = current_pawn.get_coordinates();
                    Color current_pawn_color = current_pawn.get_color();

                    add_horizontal_moves(current_pawn_coordinates, current_pawn_color, moves_list);
                    add_vertical_moves(current_pawn_coordinates, current_pawn_color, moves_list);
                    add_diagonal1_moves(current_pawn_coordinates, current_pawn_color, moves_list);
                    add_diagonal2_moves(current_pawn_coordinates, current_pawn_color, moves_list);
                    return moves_list;
            }

            bool PawnLists::check_not_blocked_vertical_path(const Coordinates& current_coord,
                                                            const Coordinates& potential_coord,
                                                            const Color& opponent_color, std::string side) const
            {
                if(side == "+")
                {
                    Coordinates move_coord = Coordinates(current_coord.x(), current_coord.y());
                    while(move_coord.x() < potential_coord.x())
                    {
                        if(find_pawn(move_coord) == opponent_color)
                            return false;
                        move_coord.set_x(move_coord.x()+1);
                    }
                }
                if(side == "-")
                {
                    Coordinates move_coord = Coordinates(current_coord.x(), current_coord.y());
                    while(move_coord.x() > potential_coord.x())
                    {
                        if(find_pawn(move_coord) == opponent_color)
                            return false;
                        move_coord.set_x(move_coord.x()-1);

                    }
                }
                return true;
            }

            void PawnLists::add_vertical_moves(const Coordinates& current_pawn_coordinates,
                                               const Color& current_pawn_color, std::vector<Move*>
                                               moves_list) const {
                int number_pawn_aligned = 0;
                Coordinates potential_new_coord_pawn;
                Color opponent_pawn_color = current_pawn_color == BLACK ? WHITE : BLACK;

                for (int i = 0; i < DEFAULT_WIDTH; i++) {
                    Color i_pawn_color = find_pawn(Coordinates(i, current_pawn_coordinates.y()));
                    if (i_pawn_color != NONE) {
                        number_pawn_aligned++;
                    }
                }

                if (current_pawn_coordinates.x() + number_pawn_aligned < DEFAULT_WIDTH) {
                    potential_new_coord_pawn = Coordinates(current_pawn_coordinates.x() + number_pawn_aligned, current_pawn_coordinates.y());

                    if(check_not_blocked_vertical_path(current_pawn_coordinates,
                                                       potential_new_coord_pawn, opponent_pawn_color, "+")) {
                        if (find_pawn(potential_new_coord_pawn) != current_pawn_color) {
                            moves_list.push_back(new Move(current_pawn_color, current_pawn_coordinates, potential_new_coord_pawn));
                        }
                    }
                }


                if(current_pawn_coordinates.x() - number_pawn_aligned >= 0 ) {
                    potential_new_coord_pawn = Coordinates(current_pawn_coordinates.x() - number_pawn_aligned, current_pawn_coordinates.y());

                    if(check_not_blocked_vertical_path(current_pawn_coordinates,
                                                       potential_new_coord_pawn, opponent_pawn_color, "-")) {
                        if (find_pawn(potential_new_coord_pawn) != current_pawn_color) {
                            moves_list.push_back(new Move(current_pawn_color, current_pawn_coordinates, potential_new_coord_pawn));
                        }
                    }
                }
            }

            bool PawnLists::check_not_blocked_horizontal_path(const Coordinates& current_coord,
                                            const Coordinates& potential_coord,
                                            const Color& opponent_color, std::string side) const
            {
                if(side == "+")
                        {
                            Coordinates move_coord = Coordinates(current_coord.x(), current_coord.y());
                            while(move_coord.y() < potential_coord.y())
                            {
                                if(find_pawn(move_coord) == opponent_color)
                                    return false;
                                move_coord.set_y(move_coord.y()+1);
                            }
                        }
                        if(side == "-")
                        {
                            Coordinates move_coord = Coordinates(current_coord.x(), current_coord.y());
                            while(move_coord.y() > potential_coord.y())
                            {
                                if(find_pawn(move_coord) == opponent_color)
                                    return false;
                                move_coord.set_y(move_coord.y()-1);

                            }
                        }
                        return true;
            }

            void PawnLists::add_horizontal_moves(const Coordinates& current_pawn_coordinates,
                                    const Color& current_pawn_color, std::vector<Move*>
                                    moves_list) const
            {
                int number_pawn_aligned = 0;
                Coordinates potential_new_coord_pawn;
                Color opponent_pawn_color = current_pawn_color == BLACK ? WHITE : BLACK;

                for (int i = 0; i < DEFAULT_HEIGHT; i++) {
                    if (find_pawn(Coordinates(current_pawn_coordinates.x(), i)) != NONE) {
                        number_pawn_aligned++;
                    }
                }

                if (current_pawn_coordinates.y() + number_pawn_aligned < DEFAULT_HEIGHT) {
                    potential_new_coord_pawn = Coordinates(current_pawn_coordinates.x(), current_pawn_coordinates.y() + number_pawn_aligned);
                    if(check_not_blocked_horizontal_path(current_pawn_coordinates,
                                                              potential_new_coord_pawn, opponent_pawn_color, "+")) {
                        if (find_pawn(potential_new_coord_pawn) != current_pawn_color) {
                            moves_list.push_back(new Move(current_pawn_color, current_pawn_coordinates, potential_new_coord_pawn));
                        }
                    }
                }

                if (current_pawn_coordinates.y() - number_pawn_aligned >= 0) {
                    potential_new_coord_pawn = Coordinates(current_pawn_coordinates.x(), current_pawn_coordinates.y() - number_pawn_aligned);
                    if(check_not_blocked_horizontal_path(current_pawn_coordinates,
                                                              potential_new_coord_pawn, opponent_pawn_color, "-")) {
                        if (find_pawn(potential_new_coord_pawn) != current_pawn_color) {
                            moves_list.push_back(new Move(current_pawn_color, current_pawn_coordinates, potential_new_coord_pawn));
                        }
                    }
                }
            }

            bool PawnLists::check_not_blocked_diagonal1_path(const Coordinates& current_coord,
                                            const Coordinates& potential_coord,
                                            const Color& opponent_color, std::string side) const
            {
                if(side == "+")
                {
                    Coordinates move_coord = Coordinates(current_coord.x(), current_coord.y());
                    while(move_coord.x() < potential_coord.x() && move_coord.y() < potential_coord.y())
                    {
                        if(find_pawn(move_coord) == opponent_color)
                            return false;
                        move_coord.set_x(move_coord.x()+1);
                        move_coord.set_y(move_coord.y()+1);
                    }
                }
                if(side == "-")
                {
                    Coordinates move_coord = Coordinates(current_coord.x(), current_coord.y());
                    while(move_coord.x() > potential_coord.x() && move_coord.y() > potential_coord.y())
                    {
                        if(find_pawn(move_coord) == opponent_color)
                            return false;
                        move_coord.set_x(move_coord.x()-1);
                        move_coord.set_y(move_coord.y()-1);

                    }
                }
                return true;
            }

            void PawnLists::add_diagonal1_moves(const Coordinates& current_pawn_coordinates,
                                    const Color& current_pawn_color, std::vector<Move*>
                                    moves_list) const
            {
                int number_pawn_aligned = 0;
                Coordinates potential_new_coord_pawn;
                Color opponent_pawn_color = current_pawn_color == BLACK ? WHITE : BLACK;
                int number_case_diagonal = current_pawn_coordinates.x() >= current_pawn_coordinates.y() ? (DEFAULT_HEIGHT - current_pawn_coordinates.x() + current_pawn_coordinates.y()) : (DEFAULT_WIDTH - current_pawn_coordinates.y() + current_pawn_coordinates.x());

                Coordinates coord_potential_pawn = Coordinates(current_pawn_coordinates.x(), current_pawn_coordinates.y());

                while(coord_potential_pawn.x() != 0 && coord_potential_pawn.y() != 0){
                    coord_potential_pawn.set_x(coord_potential_pawn.x()-1);
                    coord_potential_pawn.set_y(coord_potential_pawn.y()-1);
                }

                for (int i = 0; i < number_case_diagonal; i++) {
                    if (find_pawn(coord_potential_pawn) != NONE) {
                        number_pawn_aligned++;
                    }
                    coord_potential_pawn.set_x(coord_potential_pawn.x()+1);
                    coord_potential_pawn.set_y(coord_potential_pawn.y()+1);
                }

                if ((current_pawn_coordinates.x() + number_pawn_aligned < DEFAULT_WIDTH) && (current_pawn_coordinates.y() + number_pawn_aligned < DEFAULT_HEIGHT)) {
                    potential_new_coord_pawn = Coordinates(current_pawn_coordinates.x() + number_pawn_aligned, current_pawn_coordinates.y() + number_pawn_aligned);
                    if(check_not_blocked_diagonal1_path(current_pawn_coordinates,
                                                             potential_new_coord_pawn, opponent_pawn_color, "+")) {
                        if (find_pawn(potential_new_coord_pawn) != current_pawn_color) {
                            moves_list.push_back(new Move(current_pawn_color, current_pawn_coordinates, potential_new_coord_pawn));
                        }
                    }
                }

                if ((current_pawn_coordinates.x() - number_pawn_aligned >= 0) && (current_pawn_coordinates.y() - number_pawn_aligned >= 0)) {
                    potential_new_coord_pawn = Coordinates(current_pawn_coordinates.x() - number_pawn_aligned, current_pawn_coordinates.y() - number_pawn_aligned);
                    if(check_not_blocked_diagonal1_path(current_pawn_coordinates,
                                                             potential_new_coord_pawn, opponent_pawn_color, "-")) {
                        if (find_pawn(potential_new_coord_pawn) != current_pawn_color) {
                            moves_list.push_back(new Move(current_pawn_color, current_pawn_coordinates, potential_new_coord_pawn));
                        }
                    }
                }
            }

            bool PawnLists::check_not_blocked_diagonal2_path(const Coordinates& current_coord,
                                            const Coordinates& potential_coord,
                                            const Color& opponent_color, std::string side) const
            {
                if(side == "+")
                {
                    Coordinates move_coord = Coordinates(current_coord.x(), current_coord.y());
                    while(move_coord.x() < potential_coord.x() && move_coord.y() > potential_coord.y())
                    {
                        if(find_pawn(move_coord) == opponent_color)
                            return false;
                        move_coord.set_x(move_coord.x()+1);
                        move_coord.set_y(move_coord.y()-1);
                    }
                }
                if(side == "-")
                {
                    Coordinates move_coord = Coordinates(current_coord.x(), current_coord.y());
                    while(move_coord.x() > potential_coord.x() && move_coord.y() < potential_coord.y())
                    {
                        if(find_pawn(move_coord) == opponent_color)
                            return false;
                        move_coord.set_x(move_coord.x()-1);
                        move_coord.set_y(move_coord.y()+1);

                    }
                }
                return true;
            }

            void PawnLists::add_diagonal2_moves(const Coordinates& current_pawn_coordinates,
                                    const Color& current_pawn_color, std::vector<Move*>
                                    moves_list) const
            {
                int number_pawn_aligned = 0;
                Coordinates potential_new_coord_pawn;
                Color opponent_pawn_color = current_pawn_color == BLACK ? WHITE : BLACK;
                int number_case_diagonal = (current_pawn_coordinates.x() + current_pawn_coordinates.y() + 1) < DEFAULT_WIDTH ? (current_pawn_coordinates.x() + current_pawn_coordinates.y() + 1) : (DEFAULT_WIDTH * 2 - 1 - current_pawn_coordinates.x() - current_pawn_coordinates.y());
                Coordinates coord_potential_pawn = Coordinates(current_pawn_coordinates.x(), current_pawn_coordinates.y());

                while (coord_potential_pawn.x() != 7 && coord_potential_pawn.y() != 0) {
                    coord_potential_pawn.set_x(coord_potential_pawn.x()+1);
                    coord_potential_pawn.set_y(coord_potential_pawn.y()-1);
                }

                for (int i = 0; i < number_case_diagonal; i++) {
                    if (find_pawn(coord_potential_pawn) != NONE) {
                        number_pawn_aligned++;
                    }
                    coord_potential_pawn.set_x(coord_potential_pawn.x()-1);
                    coord_potential_pawn.set_y(coord_potential_pawn.y()+1);
                }

                if ((current_pawn_coordinates.x() + number_pawn_aligned < DEFAULT_WIDTH) && (current_pawn_coordinates.y() - number_pawn_aligned >= 0)) {
                    potential_new_coord_pawn = Coordinates(current_pawn_coordinates.x() + number_pawn_aligned, current_pawn_coordinates.y() - number_pawn_aligned);
                    if(check_not_blocked_diagonal2_path(current_pawn_coordinates,
                                                             potential_new_coord_pawn, opponent_pawn_color, "+")) {
                        if (find_pawn(potential_new_coord_pawn) != current_pawn_color) {
                            moves_list.push_back(new Move(current_pawn_color, current_pawn_coordinates, potential_new_coord_pawn));
                        }
                    }
                }

                if ((current_pawn_coordinates.x() - number_pawn_aligned >= 0) && (current_pawn_coordinates.y() + number_pawn_aligned < DEFAULT_HEIGHT)) {
                    potential_new_coord_pawn = Coordinates(current_pawn_coordinates.x() - number_pawn_aligned, current_pawn_coordinates.y() + number_pawn_aligned);
                    if(check_not_blocked_diagonal2_path(current_pawn_coordinates,
                                                             potential_new_coord_pawn, opponent_pawn_color, "-")) {
                        if (find_pawn(potential_new_coord_pawn) != current_pawn_color) {
                            moves_list.push_back(new Move(current_pawn_color, current_pawn_coordinates, potential_new_coord_pawn));
                        }
                    }
                }
            }

            }
        }
    }
}
