/**
 * @file openxum/core/games/lines_of_action/connected_pawns.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2019 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vector>
#include <openxum/core/games/lines_of_action/connected_pawns.hpp>
#include <openxum/core/games/lines_of_action/pawn.hpp>
#include <openxum/core/games/lines_of_action/board.hpp>
#include <openxum/core/games/lines_of_action/color.hpp>

namespace openxum {
    namespace core {
        namespace games {
            namespace lines_of_action {

            ConnectedPawns::ConnectedPawns(const Pawn& last_pawn_moved, int count_of_pawn, std::vector<std::vector<Pawn*> > board){
                _board = board;
                _lpm = last_pawn_moved;
                _pawns_color = last_pawn_moved.get_color();
                _count_of_pawn = count_of_pawn;
            }

           ConnectedPawns::~ConnectedPawns() {
                for (auto neighbor: _neighbors) {
                    delete neighbor;
                }
                for (auto visited: _visited) {
                    delete visited;
                }
                for (auto wait: _wait) {
                    delete wait;
                }
            }

            void ConnectedPawns::_get_neighbors_of(const Pawn& current_pawn){
                _neighbors.clear();
                int current_x;
                int current_y;

                for(int i = -1; i <= 1; i++) {
                    for (int j = -1; j <= 1; j++) {
                        current_x = current_pawn.get_coordinates().x() + i;
                        current_y = current_pawn.get_coordinates().y() + j;

                        if(current_x >= 0 && current_x < DEFAULT_WIDTH &&
                                current_y >= 0 && current_y < DEFAULT_HEIGHT){

                            Pawn neighbor_pawn = *_board[current_x][current_y];

                            if(neighbor_pawn.get_color() == _pawns_color &&
                                    _is_visited(neighbor_pawn) == false &&
                                    _is_waiting(neighbor_pawn) == false &&
                                    (i != 0 || j != 0)) {
                                _neighbors.push_back(_board[current_x][current_y]);
                            }
                        }
                    }
                }
            }

            bool ConnectedPawns::_is_visited(const Pawn& pawn){
                for(unsigned int i = 0; i < _visited.size(); i++){
                    if(_visited[i]->get_coordinates().x() == pawn.get_coordinates().x() &&
                            _visited[i]->get_coordinates().y() == pawn.get_coordinates().y() &&
                            _visited[i]->get_color() == pawn.get_color()){
                        return true;
                    }
                }
                return false;
            }

            bool ConnectedPawns::_is_waiting(const Pawn& pawn){
                for(unsigned int i = 0; i < _wait.size(); i++){
                    if(_wait[i]->get_coordinates().x() == pawn.get_coordinates().x() &&
                            _wait[i]->get_coordinates().y() == pawn.get_coordinates().y() &&
                            _wait[i]->get_color() == pawn.get_color()){
                        return true;
                    }
                }
                return false;
            }

            void ConnectedPawns::_add_wait(){

                if(_neighbors.empty()){return;}

                for(unsigned int i = 0; i < _neighbors.size(); i++){
                    _wait.push_back(_neighbors[i]);
                }
            }

            bool ConnectedPawns::_wait_is_empty(){
                return (_wait.empty());
            }

           Pawn ConnectedPawns::_pop_wait(){
                if(!_wait.empty())
                {
                    Pawn pawn = *_wait.back();
                    _wait.pop_back();
                    return pawn;
                }
                return Pawn(NONE, Coordinates(-1, -1));
            }

           bool ConnectedPawns::all_pawns_connected(){
               Pawn current_pawn = _lpm;
               _get_neighbors_of(current_pawn);
               _visited.push_back(&current_pawn);
               _add_wait();

               while(!_wait.empty()) {
                   current_pawn = _pop_wait();
                   _get_neighbors_of(current_pawn);
                   _add_wait();
                   _visited.push_back(&current_pawn);
               }

               return (_visited.size() == (unsigned int)_count_of_pawn);
           }
           }
        }
    }
}
