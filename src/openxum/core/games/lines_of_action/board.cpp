/**
 * @file openxum/core/games/lines_of_action/state.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2019 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <openxum/core/games/lines_of_action/board.hpp>
#include <openxum/core/games/lines_of_action/coordinates.hpp>
#include <openxum/core/games/lines_of_action/pawn_list.hpp>
#include <openxum/core/games/lines_of_action/pawn.hpp>
#include <openxum/core/games/lines_of_action/dimension.hpp>
#include <openxum/core/games/lines_of_action/color.hpp>
#include <openxum/core/games/lines_of_action/connected_pawns.hpp>

namespace openxum {
    namespace core {
        namespace games {
            namespace lines_of_action {
            Board::Board(const PawnLists& pl){
                _pawnList = PawnLists(pl);
                _width = DEFAULT_WIDTH;
                _height = DEFAULT_HEIGHT;

                for (int i = 0; i < DEFAULT_WIDTH; i++) {
                    std::vector<Pawn*> vCol;

                    for(int j = 0; j < DEFAULT_HEIGHT; j++){

                        Coordinates current_coordinates  = Coordinates(i,j);
                        Color current_color = _pawnList.find_pawn(current_coordinates);
                        vCol.push_back(new Pawn(current_color, current_coordinates));
                    }
                    _board.push_back(vCol);
                }
            }

            int Board::getHeight() const { return DEFAULT_HEIGHT; }
            int Board::getWidth() const { return DEFAULT_WIDTH; }
            PawnLists Board::pawnlists() { return _pawnList; }

            Pawn Board::get_pawn_at(int i, int j) const {
                return *_board[i][j];
            }

            void Board::move_pawn(const Move& move){
                Color color_move_from = move.color();
                Color color_move_to = _pawnList.find_pawn(move.to());

                if(color_move_to == NONE){
                    _pawnList.change_coord_pawn(move.from(), move.to());
                }

                if(color_move_from == WHITE && color_move_to == BLACK){
                    _pawnList.delete_pawn(move.to());
                    _pawnList.change_coord_pawn(move.from(), move.to());
                }

                if(color_move_from == BLACK && color_move_to == WHITE){
                    _pawnList.delete_pawn(move.to());
                    _pawnList.change_coord_pawn(move.from(), move.to());
                }
                _board[move.from().x()][move.from().y()]->set_color(NONE);
                _board[move.to().x()][move.to().y()]->set_color(move.color());
            }

            void Board::delete_pawn(const Coordinates& coord)
            {
                _pawnList.delete_pawn(coord);
                _board[coord.x()][coord.y()]->set_color(NONE);
            }

            openxum::core::common::Moves Board::get_possible_moves_of_player(const Color& color_player) const{
                return _pawnList.get_possible_moves_list_player(color_player);
            }

            openxum::core::common::Moves Board::get_possible_moves_of(const Pawn& pawn) const{
                std::vector<Move*> old_moves= _pawnList.get_moves_of_current_pawn(pawn);
                openxum::core::common::Moves new_moves;
                for(unsigned int i = 0; i < old_moves.size(); i++)
                {
                    new_moves.push_back(old_moves[i]);
                }
                return new_moves;
                }

            Pawn* Board::pick_a_pawn_of(const Color& color) const{
                for(int i = 0; i < DEFAULT_WIDTH; i++){
                    for(int j = 0; j < DEFAULT_HEIGHT; j++){
                        if(get_pawn_at(i,j).get_color() == color){
                            return _board[i][j];
                        }
                    }
                }
                return new Pawn(NONE, Coordinates(-1,-1));
            }

            bool Board::is_winning_pawn(const Pawn& last_pawn_moved) const{
                int count_of_pawn;

                if (last_pawn_moved.get_color() == BLACK) {
                    count_of_pawn = _pawnList.count_black_pawn();
                }
                else if (last_pawn_moved.get_color() == WHITE) {
                    count_of_pawn = _pawnList.count_white_pawn();
                }

                if(count_of_pawn == 1) {return true;}

                ConnectedPawns connected_pawn = ConnectedPawns(last_pawn_moved, count_of_pawn, _board);

                return connected_pawn.all_pawns_connected();
            }
            }
        }
    }
}
